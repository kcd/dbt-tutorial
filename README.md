# Jaffle shop tutorial

Setup Big Query then jump to the CLI instructions in [tutorial setting up](https://docs.getdbt.com/tutorial/setting-up/) and [create a project](https://docs.getdbt.com/tutorial/create-a-project-dbt-cli#create-a-project)

For documentation about the project see the [overview.md](jaffle-shop/models/overview.md)

## Using this VS code dev container

Ensure you use the standard `~/.dbt` folder update additional secret key files as follows, with your username as the default for non-docker execution.

    keyfile: "{{ env_var('DBT_PROFILES_DIR', '/Users/kdyer/.dbt') }}/dbt-user-creds.json"



## Experimental extras

Experimented with cleaning up old models. It is safe to run:

    dbt run-operation drop_old_relations

Which will list objects which don't match the model names and should work for Snowflake and BigQuery.
To do it for real it will instruct you to append `--args '{dryrun: False}'`. Be careful.