{% macro drop_old_relations(dryrun=True) %}

{% if execute %}
  {% set current_models=[] %}

  {% for node in graph.nodes.values()
     | selectattr("resource_type", "in", ["model", "seed", "snapshot"])%}
      {% set object_name = node.alias if node.alias else node.name %}
      {# do log('Found [' + object_name + ']', True) #}
      {% do current_models.append(object_name) %}
  {% endfor %}
{% endif %}

{% set cleanup_query %}

  WITH MODELS_TO_DROP AS (
    SELECT
      CASE
        WHEN TABLE_TYPE = 'BASE TABLE' THEN 'TABLE'
        WHEN TABLE_TYPE = 'VIEW' THEN 'VIEW'
      END AS RELATION_TYPE,
    {% if target.type == 'bigquery' %}
      CONCAT('`', TABLE_CATALOG, '.', TABLE_SCHEMA, '.', TABLE_NAME, '`') AS RELATION_NAME
    {% else %}
      CONCAT_WS('.', TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME) AS RELATION_NAME
    {% endif %}
    FROM
      {{ target.dataset if target.type == 'bigquery' else target.database }}.INFORMATION_SCHEMA.TABLES
    WHERE TABLE_SCHEMA = '{{ target.schema }}'
      AND TABLE_NAME NOT IN
        ({%- for model in current_models -%}
            '{{ model if target.type == 'bigquery' else model.upper() }}'
            {%- if not loop.last -%}
                ,
            {% endif %}
        {%- endfor -%}))
  SELECT
    'DROP ' || RELATION_TYPE || ' ' || RELATION_NAME || ';'  as DROP_COMMANDS
  FROM
    MODELS_TO_DROP
  -- intentionally exclude unhandled table_types, including 'external table`
  WHERE RELATION_TYPE is not null

{% endset %}

{# do log(cleanup_query, info=True) #}
{% set drop_commands = run_query(cleanup_query).columns[0].values() %}

{% if drop_commands %}
  {% if dryrun | as_bool == False %}
    {% do log('Executing DROP commands', True) %}
  {% else %}
    {% do log("Dry run of DROP commands, execute with --args '{dryrun: False}'", True) %}
  {% endif %}
  {% for drop_command in drop_commands %}
    {% do log(drop_command, True) %}
    {% if dryrun | as_bool == False %}
      {% do run_query(drop_command) %}
    {% endif %}
  {% endfor %}
{% else %}
  {% do log('No relations to clean.', True) %}
{% endif %}

{%- endmacro -%}