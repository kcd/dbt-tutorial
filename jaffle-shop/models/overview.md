{% docs __overview__ %}
# Jaffle Shop Tutorial

By creating this file with {% raw %}`{% docs __overview__ %}`{% endraw %} I am replacing the placeholder docs homepage.

All models should have documentation, the recommendation is each `<model_name>.sql` has a corresponding `<model_name>.yml`. The following will list all missing documentation.

    dbt run-operation required_docs

{% enddocs %}